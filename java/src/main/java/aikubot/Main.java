package aikubot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, "Tosbaaa", botKey, true, true);
        //new Main(reader, writer, "Tosbaaa", botKey, false);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, String name, String key, boolean isJoin, boolean IsCreateRace) throws IOException {
        this.writer = writer;
        String line = null;
        
        if(IsCreateRace)
        	send(new CreateRace(name, key, "keimola", "yuret", 1));
        else if(isJoin)
        	send(new Join(name, key));
        else
        	send(new JoinRace(name, key));
        
        Learner learner = new Learner();
        GameInitMsg gameInitMsg = null;
        boolean gameStarted = false;
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            final MsgWrapperWithGameTick msgGT = gson.fromJson(line, MsgWrapperWithGameTick.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	//System.out.println("\nCarPos Json:\n" + new Gson().toJson(msgFromServer) + "\n");
            	final CarPositionMsg carPosMsg = gson.fromJson(line, CarPositionMsg.class);
            	if(gameStarted){
            		double value = learner.process(carPosMsg);
            		send(new Throttle(value, carPosMsg.gameTick));
            	}
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	gameInitMsg = gson.fromJson(line, GameInitMsg.class);
            	learner.setRaceInfo(gameInitMsg.data.race);
                System.out.println("Race init");
                System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
                //System.out.println("Pieces length: " + pieces.length);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                gameStarted = true;
                //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
            } else if(msgFromServer.msgType.equals("crash")){
            	 System.out.println("Someone crashed");
                 //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
            }else if(msgFromServer.msgType.equals("turboAvailable")){
            	System.out.println("turboAvailable");
            }
            else if(msgFromServer.msgType.equals("lapFinished")){
            	System.out.println("lapFinished");
                //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
           }else if (msgFromServer.msgType.equals("yourCar")) {
        	   YourCarMsg yourCarMsg = gson.fromJson(line, YourCarMsg.class);
        	   learner.setCarInfo(yourCarMsg.data);
               System.out.println("My Car");
               //System.out.println("\nJson:\n" + new Gson().toJson(msgFromServer) + "\n");
           }
            else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
    	//System.out.println(msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class MsgWrapperWithGameTick {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    MsgWrapperWithGameTick(final String msgType, final Object data, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class JoinRace extends SendMsg {
    public JoinRaceData data;
    
    JoinRace(final String name, final String key) {
    	data = new JoinRaceData(name, key);
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
    
    @Override
    protected Object msgData() {
        return data;
    }
}

class CreateRace extends SendMsg{
	public class CreateRaceData{
		BotId botId;
		String trackName;
		String password;
		int carCount;
		
		public CreateRaceData(String name, String key, String trackName, String password, int carCount){
			botId = new BotId(name, key);
			this.trackName = trackName;
			this.password = password;
			this.carCount = carCount;
		}
	}
	
	public CreateRaceData data;
	
	public CreateRace(String name, String key, String trackName, String password, int carCount){
		data = new CreateRaceData(name, key, trackName, password, carCount);
	}

	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "createRace";
	}
	
	@Override
    protected Object msgData() {
        return data;
    }
	
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;
    private int gameTick;

    public Throttle(double value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;
    int gameTick;
    
    public SwitchLane(String value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}