package aikubot;

import aikubot.CarPositionMsg.CarPositionData;
import aikubot.GameInitMsg.GameInitData.Race;
import aikubot.GameInitMsg.GameInitData.Race.Car;
import aikubot.GameInitMsg.GameInitData.Race.Car.Dimensions;
import aikubot.YourCarMsg.CarDefinition;

public class Learner {
	
	public enum LearnState{
		START, FIRSTTICK, FRICTION, CALCULATEACCELERATION, GOLONGROAD, READY, CALCULATEFRICTION, ALLPARAMETERSLEARNED;
	}
	
	public class LearningData{
		public double previousSpeed = 0;
		public double prevThrottle = 0;
		public int prevPieceIndex = 0;
		public int currentPieceIndex = 0;
		public double prevInPieceDistance = 0;
		public double currentInPieceDistance = 0;
		public double totalDistance = 0;
		public double prevDistance = 0;
		public int accelerationTick = 0;
	}
	
	CarDefinition carInfo;
	Race raceInfo;
	boolean accelerationLearned = false;
	boolean maxSpeedLearned = false;
	double acceleration = 0;
	double maxSpeed = 0;
	double motorPower = 0;
	
	double currentSpeed = 0;
	LearnState state = LearnState.START;
	LearnState nextState = null;
	LearningData data;
	
	public Learner(){
		data = new LearningData();
	}
	
	public void setCarInfo(CarDefinition carInfo){
		this.carInfo = carInfo;
	}
	
	public void setRaceInfo(Race race){
		raceInfo = race;
	}
	
	public double process(CarPositionMsg msg){
		double value = 0;
		CarPositionData carPosData = getMyCarPosition(msg);
		
		data.currentInPieceDistance = carPosData.piecePosition.inPieceDistance;
		data.currentPieceIndex = carPosData.piecePosition.pieceIndex;
		
		//System.out.println("State: " + state);
		double distance = 0;
		switch(state){
		case START:
			value = 0.2;
			data.prevThrottle = value;
			data.prevPieceIndex = data.currentPieceIndex;
			data.prevInPieceDistance = data.currentInPieceDistance;
			System.out.println(state);
			//state = LearnState.CALCULATEACCELERATION;
			state = LearnState.FIRSTTICK;
			System.out.println(state);
			break;
		case FIRSTTICK:
			if(data.currentPieceIndex == data.prevPieceIndex)
				distance = data.currentInPieceDistance - data.prevInPieceDistance;
			else
				distance = data.currentInPieceDistance 
					+ raceInfo.track.pieces[data.prevPieceIndex].length - data.prevInPieceDistance;
			
			System.out.println("Prev dist: " + data.prevDistance);
			System.out.println("Curr dist: " + distance);
			value = 0.2;
			data.totalDistance += distance;
			data.accelerationTick++;
			data.prevDistance = distance;
			state = LearnState.FRICTION;
			System.out.println(state);
			data.prevInPieceDistance = data.currentInPieceDistance;
			data.prevPieceIndex = data.currentPieceIndex;
			break;
		case FRICTION:
			if(data.currentPieceIndex == data.prevPieceIndex)
				distance = data.currentInPieceDistance - data.prevInPieceDistance;
			else
				distance = data.currentInPieceDistance 
					+ raceInfo.track.pieces[data.prevPieceIndex].length - data.prevInPieceDistance;
			
			System.out.println("Prev dist: " + data.prevDistance);
			System.out.println("Curr dist: " + distance);
			
			if(Math.abs(data.prevDistance - distance) < 1e-3){
				System.out.println("Total distance: " + data.totalDistance);
				System.out.println("Total tick: " + data.accelerationTick);
			}
			data.accelerationTick++;
			data.prevDistance = distance;
			data.totalDistance += distance;
			value = 0.2;
			state = LearnState.FRICTION;
			System.out.println(state);
			data.prevInPieceDistance = data.currentInPieceDistance;
			data.prevPieceIndex = data.currentPieceIndex;
			break;
		case CALCULATEACCELERATION:
			if(data.currentPieceIndex == data.prevPieceIndex)
				distance = data.currentInPieceDistance - data.prevInPieceDistance;
			else
				distance = data.currentInPieceDistance 
					+ raceInfo.track.pieces[data.prevPieceIndex].length - data.prevInPieceDistance;
			
			System.out.println("Prev dist: " + data.prevDistance);
			System.out.println("Curr dist: " + distance);
			
			if(Math.abs(distance- data.prevDistance) < 5e-4){//Acceleration done.
				System.out.println("ACCELERATION");
				accelerationLearned = true;
				acceleration = (2*data.totalDistance*10)/Math.pow(data.accelerationTick,2);
				maxSpeed = distance*10;
				
				System.out.println("Total dist: " + data.totalDistance);
				System.out.println("Total tick: " + data.accelerationTick);
				System.out.println("Acc: " + acceleration);
				System.out.println("MaxSpeed: " + maxSpeed);
				
				value = 0.3;
				data.prevThrottle = 0.1;
				state = LearnState.GOLONGROAD;
				System.out.println(state);
				data.accelerationTick = 0;
				data.totalDistance = 0;
				data.prevDistance = distance;
			}else{
				data.accelerationTick++;
				data.totalDistance += distance;
				data.prevDistance = distance;
				value = data.prevThrottle;
				data.prevInPieceDistance = data.currentInPieceDistance;
				data.prevPieceIndex = data.currentPieceIndex;
			}
			
			break;
		case GOLONGROAD:
			boolean checkLongRoad = true;
			for(int i = 1; i <= 4; i++){
				if(raceInfo.track.pieces[data.currentPieceIndex + 1 + i].length == 0){
					checkLongRoad = false;
					break;
				}
			}
			
			if(checkLongRoad){
				value = 0.05;
				data.prevThrottle = 0.2;
				state = LearnState.READY;
				System.out.println(state);
			}
			
			value = 0.3;
			data.prevPieceIndex = data.currentPieceIndex;
			
			break;
		case READY:
			if(carPosData.angle > 0 || raceInfo.track.pieces[data.currentPieceIndex].length == 0){
				value = 0.05;
				data.prevPieceIndex = data.currentPieceIndex;
				data.prevThrottle = 0.05;
			}else{
				state = LearnState.CALCULATEFRICTION;
				System.out.println(state);
				value = 0.0;
				data.prevPieceIndex = data.currentPieceIndex;
				data.prevInPieceDistance = data.currentInPieceDistance;
				data.prevThrottle = 0.0;
			}
			break;
		case CALCULATEFRICTION:
			
			distance = 0;
			
			if(data.currentPieceIndex == data.prevPieceIndex)
				distance = data.currentInPieceDistance - data.prevInPieceDistance;
			else
				distance = data.currentInPieceDistance 
					+ raceInfo.track.pieces[data.prevPieceIndex].length - data.prevInPieceDistance;
			System.out.println("Dist: " + distance);
			if(Math.abs(distance-data.prevDistance) < 5e-4){//Acceleration done.
				System.out.println("FRICTION");
				acceleration = (2*data.totalDistance)/Math.pow(data.accelerationTick,2);
				
				System.out.println("Total dist: " + data.totalDistance);
				System.out.println("Total tick: " + data.accelerationTick);
				System.out.println("Acc: " + acceleration);
				
				value = 0.0;
				data.prevThrottle = 0.0;
				state = LearnState.ALLPARAMETERSLEARNED;
				System.out.println(state);
				data.accelerationTick = 0;
				data.totalDistance = 0;
				data.prevDistance = distance;
			}else{
				data.accelerationTick++;
				data.totalDistance += distance;
				data.prevDistance = distance;
				value = data.prevThrottle;
				data.prevInPieceDistance = data.currentInPieceDistance;
				data.prevPieceIndex = data.currentPieceIndex;
			}
			
			break;
		default:
			value = data.prevThrottle;
		}
		
		return value;
	}
	
	public LearnState getState(){
		return this.state;
	}
	
	public CarPositionData getMyCarPosition(CarPositionMsg msg){
		for(CarPositionData cp : msg.data){
			if(cp.id.color.equals(carInfo.color))
				return cp;
		}
		
		return null;
	}
	
	public Dimensions getMyCarDimensions(){
		Dimensions dim = null;
		
		for(Car c : raceInfo.cars){
			if(c.id.color.equals(carInfo.color))
				dim = c.dimensions;
		}
		
		return dim;
	}
	
	

}
