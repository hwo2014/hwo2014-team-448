package aikubot;

public class TurboMsg {
	public class TurboData{
		public double turboDurationMilliseconds;
		public int turboDurationTicks;
		public double turboFactor;
	}
	
	public String msgType;
	public TurboData data;
}
