package aikubot;

import aikubot.GameInitMsg.GameInitData.Race.Car;
import aikubot.YourCarMsg.CarDefinition;

public class CarPositionMsg {
	
	public class PiecePosition {
		public class PiecePositionLane{
			public int startIndex;
			public int endIndex;
		}
		public int pieceIndex;
		public double inPieceDistance;
		public int lap;
		public PiecePositionLane lane;

	}
	
	public class CarPositionData {
		public CarDefinition id;
		public double angle;
		public PiecePosition piecePosition;
	}
	
	public String msgType;
	public CarPositionData[] data;
	public String gameId;
	public int gameTick;
}
