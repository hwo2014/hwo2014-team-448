package aikubot;

import aikubot.YourCarMsg.CarDefinition;

import com.google.gson.annotations.SerializedName;

public class GameInitMsg {
	public class GameInitData{
		public class Race {
			public class Track {
				
				public class Lane {
					public int index;
					public double distanceFromCenter;

				}
				
				public class StartingPoint{
					public class Position{
						public double x;
						public double y;
					}
					
					public Position position;
					public double angle;
				}
				
				public class Piece {
					public double length = 0;
					public double radius = 0;
					public double angle = 0;
					
					@SerializedName("switch")
					public boolean isThereSwitch = false;

				}
				
				public String id;
				public String name;
				public Piece[] pieces;
				public Lane[] lanes;
				public StartingPoint startingPoint;
			}
			
			public class Car {
				public class Dimensions {
					public double length = 0;
					public double width = 0;
					public double guideFlagPosition;
				}
				
				public CarDefinition id;
				public Dimensions dimensions;

			}
			
			public class RaceSession{
				int laps;
				double maxLapTimeMs;
				boolean quickRace;
			}
			
			public Track track;
			public Car[] cars;
			RaceSession raceSession;

		}
		
		public Race race;
	}
	
	public String msgType;
	public GameInitData data;

}
